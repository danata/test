<?php
session_start();
include_once 'config/config.php';
// the url should be something like : http://localhost/parts/index.php/parts/view


preg_match('#(.*/)((parts|users|orders).+)#',$_SERVER['PATH_INFO'],$matches); // get the segment starting from parts/ or users/ 
 $info = $matches[2];
$uri = explode('/',$info);
$segments_count = count($uri);

error_log("segments count=".$segments_count);
error_log("info=".$info);
error_log("uri[0]=|".$uri[0]."|");

// $controller_name = isset($uri[0])? $uri[0]:"parts";
// $controller_name = trim($controller_name);
if ( isset($uri[0]) && !empty($uri[0]) )
{
    $controller_name = $uri[0];
}
else 
{
	$controller_name='parts';
}

//$method     = isset($uri[1])? $uri[1]:"index";

if ( isset($uri[1]) && !empty($uri[1]))
{
   $method = $uri[1];
}
else
{
	$method='index';
}


error_log ("2 controller_name=".$controller_name);
if ( isset( $_POST['params'] ))  // for add,update,order actions coming from ajax
{
  $params = $_POST['params'];
}
else if ( isset ($_POST['search_part_id']) ) // coming from search form 
{
   $params = $_POST['search_part_id'];
}
else if ( isset($_POST['email']) &&  isset($_POST['password']) )
{
	$params = prepare_login_register_params();
}
else if ( isset($_GET['term']))
{
	$params = trim(strip_tags($_GET['term'])); //for the autocomplete component
}
else if ( $segments_count > 2)
{
	$params = array();
	for($i=2; $i <$segments_count;$i++)
	{
		$params[] = $uri[$i];
	}
}
else
{
	$params=null;
}
$params_str = var_export($params,true);
error_log("params_str=".$params_str);
error_log("controller=".$controller_name." method=".$method);


include_once 'controllers/'.$controller_name.'.php';

 try
 {
	if (! isset($_SESSION['user_type']) && $controller_name !='users')
	{
		header("Location: ".$PATH_TO_INDEX."/users/");
		die();	
	}
	$controller = new $controller_name;
	$controller->$method($params);
 }
 catch(Exception $e)
 {
	error_log($e->getMessage());	
 }

 function prepare_login_register_params()
 {	
	$email = $_POST['email'];
	$password = $_POST['password'];
	$params = array($email,$password);
	
	$confirm_pass = isset($_POST['confirm_pass'])? $_POST['confirm_pass']:"";
	$role = isset($_POST['role']) ? $_POST['role']:"";
	$name = isset($_POST['name']) ? $_POST['name']:"";
	$params[] =$confirm_pass;
	$params[]= $role;
	$params[] = $name;
	return($params);
 }

?>