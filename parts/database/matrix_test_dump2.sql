-- phpMyAdmin SQL Dump
-- version 3.3.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306

-- Generation Time: Dec 09, 2017 at 06:25 PM
-- Server version: 5.1.49
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `matrix_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `model` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`model`),
  KEY `model` (`model`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=14 ;

--
-- Dumping data for table `car`
--

INSERT INTO `car` VALUES(1, 'Audi', 2010);
INSERT INTO `car` VALUES(2, 'Bentley', 2015);
INSERT INTO `car` VALUES(3, 'BMW', 2017);
INSERT INTO `car` VALUES(4, 'Cadilac', 2014);
INSERT INTO `car` VALUES(5, 'Chevrolet', 2009);
INSERT INTO `car` VALUES(6, 'Chrysler', 2012);
INSERT INTO `car` VALUES(7, 'Citroen', 2013);
INSERT INTO `car` VALUES(8, 'Ferrari', 2016);
INSERT INTO `car` VALUES(9, 'Fiat', 2010);
INSERT INTO `car` VALUES(10, 'Ford', 2011);
INSERT INTO `car` VALUES(11, 'Honda', 2012);
INSERT INTO `car` VALUES(12, 'Kia', 2013);
INSERT INTO `car` VALUES(13, 'Mazda', 2015);

-- --------------------------------------------------------

--
-- Table structure for table `car_parts`
--

CREATE TABLE `car_parts` (
  `part_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  PRIMARY KEY (`part_id`,`car_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `car_parts`
--

INSERT INTO `car_parts` VALUES(1, 2);
INSERT INTO `car_parts` VALUES(1, 5);
INSERT INTO `car_parts` VALUES(1, 7);
INSERT INTO `car_parts` VALUES(1, 16);
INSERT INTO `car_parts` VALUES(2, 10);
INSERT INTO `car_parts` VALUES(2, 11);
INSERT INTO `car_parts` VALUES(3, 1);
INSERT INTO `car_parts` VALUES(3, 2);
INSERT INTO `car_parts` VALUES(4, 10);
INSERT INTO `car_parts` VALUES(4, 11);
INSERT INTO `car_parts` VALUES(4, 12);
INSERT INTO `car_parts` VALUES(5, 2);
INSERT INTO `car_parts` VALUES(5, 3);
INSERT INTO `car_parts` VALUES(5, 4);
INSERT INTO `car_parts` VALUES(6, 2);
INSERT INTO `car_parts` VALUES(6, 4);
INSERT INTO `car_parts` VALUES(6, 7);
INSERT INTO `car_parts` VALUES(6, 8);
INSERT INTO `car_parts` VALUES(7, 5);
INSERT INTO `car_parts` VALUES(7, 6);
INSERT INTO `car_parts` VALUES(8, 3);
INSERT INTO `car_parts` VALUES(8, 4);
INSERT INTO `car_parts` VALUES(9, 7);
INSERT INTO `car_parts` VALUES(9, 10);
INSERT INTO `car_parts` VALUES(10, 2);
INSERT INTO `car_parts` VALUES(10, 3);
INSERT INTO `car_parts` VALUES(10, 8);
INSERT INTO `car_parts` VALUES(11, 7);
INSERT INTO `car_parts` VALUES(11, 9);
INSERT INTO `car_parts` VALUES(12, 3);
INSERT INTO `car_parts` VALUES(12, 4);
INSERT INTO `car_parts` VALUES(12, 6);
INSERT INTO `car_parts` VALUES(13, 11);
INSERT INTO `car_parts` VALUES(13, 15);
INSERT INTO `car_parts` VALUES(14, 5);
INSERT INTO `car_parts` VALUES(14, 10);
INSERT INTO `car_parts` VALUES(15, 8);
INSERT INTO `car_parts` VALUES(15, 9);
INSERT INTO `car_parts` VALUES(16, 2);
INSERT INTO `car_parts` VALUES(16, 11);
INSERT INTO `car_parts` VALUES(19, 1);
INSERT INTO `car_parts` VALUES(19, 2);
INSERT INTO `car_parts` VALUES(19, 4);
INSERT INTO `car_parts` VALUES(20, 2);
INSERT INTO `car_parts` VALUES(20, 13);
INSERT INTO `car_parts` VALUES(21, 6);
INSERT INTO `car_parts` VALUES(21, 7);
INSERT INTO `car_parts` VALUES(22, 3);
INSERT INTO `car_parts` VALUES(22, 12);
INSERT INTO `car_parts` VALUES(23, 10);
INSERT INTO `car_parts` VALUES(23, 12);
INSERT INTO `car_parts` VALUES(24, 3);
INSERT INTO `car_parts` VALUES(24, 4);
INSERT INTO `car_parts` VALUES(25, 10);
INSERT INTO `car_parts` VALUES(25, 11);
INSERT INTO `car_parts` VALUES(26, 2);
INSERT INTO `car_parts` VALUES(26, 3);
INSERT INTO `car_parts` VALUES(27, 2);
INSERT INTO `car_parts` VALUES(27, 4);
INSERT INTO `car_parts` VALUES(28, 2);
INSERT INTO `car_parts` VALUES(28, 3);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` datetime NOT NULL,
  `part_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `quantity` int(11) NOT NULL,
  `sale_price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `part_id` (`part_id`,`customer_id`,`quantity`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=20 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` VALUES(5, '2017-12-07 18:33:45', 6, 1, 'Mark', 5, 175);
INSERT INTO `order` VALUES(14, '2017-12-09 08:53:27', 3, 8, 'Yasmin', 10, 550);
INSERT INTO `order` VALUES(8, '2017-12-08 13:30:34', 7, 1, 'Mark', 3, 231);
INSERT INTO `order` VALUES(7, '2017-12-08 13:22:21', 7, 1, 'Mark', 3, 231);
INSERT INTO `order` VALUES(9, '2017-12-08 13:39:05', 3, 1, 'Mark', 2, 110);
INSERT INTO `order` VALUES(10, '2017-12-08 13:43:50', 7, 1, 'Mark', 3, 231);
INSERT INTO `order` VALUES(15, '2017-12-09 08:56:24', 6, 8, 'Yasmin', 1, 35);
INSERT INTO `order` VALUES(12, '2017-12-08 15:07:41', 4, 1, 'Mark', 3, 99);
INSERT INTO `order` VALUES(16, '2017-12-09 08:57:38', 4, 8, 'Yasmin', 20, 660);
INSERT INTO `order` VALUES(17, '2017-12-09 09:38:15', 14, 8, 'Yasmin', 1, 50);
INSERT INTO `order` VALUES(18, '2017-12-09 16:14:20', 3, 8, 'Yasmin', 1, 55);
INSERT INTO `order` VALUES(19, '2017-12-09 16:14:48', 3, 8, 'Yasmin', 2, 110);

-- --------------------------------------------------------

--
-- Table structure for table `part`
--

CREATE TABLE `part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `price` int(11) NOT NULL,
  `details` text CHARACTER SET utf8 NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`price`,`quantity`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=29 ;

--
-- Dumping data for table `part`
--

INSERT INTO `part` VALUES(1, 'Front Right Outer door 22', 55, 'my description', 22);
INSERT INTO `part` VALUES(2, 'Front Right Side Door Glass', 30, 'front description', 7);
INSERT INTO `part` VALUES(3, 'Battery Cable 55', 55, 'battery description22', 7);
INSERT INTO `part` VALUES(4, 'Distributor', 33, 'Description of distributor', 21);
INSERT INTO `part` VALUES(5, 'Fuel level sensor updated', 80, 'fuel descript 3333', 5);
INSERT INTO `part` VALUES(6, 'Electronic timing controller', 35, 'electronic edited', 5);
INSERT INTO `part` VALUES(7, 'Coolant temperature sensor', 60, 'Bla', 24);
INSERT INTO `part` VALUES(8, 'Air bag control module', 32, 'air bug description cc', 8);
INSERT INTO `part` VALUES(9, 'Central locking system', 30, 'Bla', 4);
INSERT INTO `part` VALUES(10, 'Engine computer and management system', 250, 'cccccccccc\ndddddddd\neee', 8);
INSERT INTO `part` VALUES(11, 'Radiator pressure cap', 20, 'Bla', 4);
INSERT INTO `part` VALUES(12, 'Exhaust manifold', 44, 'Bla4777777777', 11);
INSERT INTO `part` VALUES(13, 'Clutch cable', 30, 'Bla', 1);
INSERT INTO `part` VALUES(14, 'Brake pump', 50, 'bla', 4);
INSERT INTO `part` VALUES(15, 'ABS Motor Circuit', 20, 'Bla', 6);
INSERT INTO `part` VALUES(16, 'Seat cover', 80, 'seat cover desc', 5);
INSERT INTO `part` VALUES(19, 'Rotor', 20, 'my new rotor', 10);
INSERT INTO `part` VALUES(20, 'Left mirror', 20, 'bla mirror bla', 30);
INSERT INTO `part` VALUES(21, 'Weel', 30, 'car weel', 6);
INSERT INTO `part` VALUES(22, 'NewPart', 60, 'basasfda', 4);
INSERT INTO `part` VALUES(23, 'Window', 20, 'sdfsadfsd ', 5);
INSERT INTO `part` VALUES(24, 'Seat belt', 20, 'description', 22);
INSERT INTO `part` VALUES(25, 'testpart', 34, '123', 22);
INSERT INTO `part` VALUES(26, 'anotherpart', 20, 'fasfsd', 30);
INSERT INTO `part` VALUES(27, 'bbbccc', 40, 'bbb desc', 5);
INSERT INTO `part` VALUES(28, 'vvv', 70, 'fff', 4);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(40) COLLATE utf8_bin NOT NULL,
  `user_type` enum('CUSTOMER','MANAGER') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=14 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` VALUES(1, 'Mark Twain', 'mark@bookstore.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CUSTOMER');
INSERT INTO `user` VALUES(2, 'John Doe', 'john@markets.com', '81dc9bdb52d04dc20036dbd8313ed055', 'MANAGER');
INSERT INTO `user` VALUES(3, 'Dana Tal', 'dana@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'MANAGER');
INSERT INTO `user` VALUES(4, 'Blue Dot', 'dot@shtrudel.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CUSTOMER');
INSERT INTO `user` VALUES(5, 'Lilach Cohen', 'lilach@shtrudel.com', 'd41d8cd98f00b204e9800998ecf8427e', 'MANAGER');
INSERT INTO `user` VALUES(6, 'marta', 'marta@shtru.com', 'd41d8cd98f00b204e9800998ecf8427e', 'MANAGER');
INSERT INTO `user` VALUES(8, 'Yasmin', 'yasmin@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CUSTOMER');
INSERT INTO `user` VALUES(9, 'Debra', 'debra@markets.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CUSTOMER');
INSERT INTO `user` VALUES(10, 'Dror', 'dror@markets.com', '81dc9bdb52d04dc20036dbd8313ed055', 'MANAGER');
INSERT INTO `user` VALUES(11, 'Nora', 'nora@markets.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CUSTOMER');
INSERT INTO `user` VALUES(12, 'Customer Test', 'customer@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CUSTOMER');
INSERT INTO `user` VALUES(13, 'Nurit', 'nurit@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CUSTOMER');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
