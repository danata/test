
<h4>Login</h4>
<?php if ( isset($error) ) { echo $error."<br>"; } ?> 
<form class="form-horizontal" role="form" method="POST" id="login" action="login" > 

  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-5">
         <input type="email" class="form-control" id="email" name="email" placeholder="Email">
    </div>
  </div>

  <div class="form-group">
    <label for="password" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-5">
         <input type="password" class="form-control" id="passowrd" name="password" placeholder="Password">
    </div>
  </div>
  
   <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Sign in</button>
    </div>
  </div>

</form>
