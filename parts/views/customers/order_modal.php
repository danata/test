
<div class="modal fade" id="order_modal" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="orderModalLabel">Your Order</h4>
      </div>
      <div class="modal-body">
        
		<div id="feedback"></div>
		<div id="order_id"></div>
		<p>
		<table class="table table-striped table-bordered table-hover" id="part_props">
		    <tr class="success"><td colspan="2">Order Details</td></tr>
			<tr><td style="width:30%;">Part Name: </td><td id="part_name"></td></tr>
			<tr><td>Description: </td><td id="part_desc"></td></tr>
			<tr><td>Price:</td><td id="price"></td></tr>
			<tr><td>Available Quantity:</td><td id="avail_quantity"></td></tr>
			<tr><td>Compatible Cars:</td><td id="compat_cars"></td></tr>
		</table>
		
		<form method="POST" id="order_form" action="order" class="form-horizontal" > 
		
            <div class="form-group">
                <label for="customer_name" class="col-sm-3 control-label text-primary " style="text-align:left;">Customer Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Name">
                </div>
            </div>
												      
			<div class="form-group">
                <label for="quantity" class="col-sm-3 control-label text-primary" style="text-align:left;">Quantity:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
                </div>
            </div>
						
			<input type="hidden" id="part_id" name="part_id" value="">
			<input type="hidden" id="customer_id" name="customer_id" value="">
			<input type="hidden" id="price_val" name="price_val" value="">
			<input type="hidden" id="status" name="status" value="">
            <button type="submit" id="order" class="btn btn-primary">Order</button>
        </form>

		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->