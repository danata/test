<?php 

function get_car_object($row)
{
	$car_obj = new stdClass();
	$car_obj->id = $row['car_id'];
	$car_obj->name = $row['car_name'];		   
	$car_obj->model = $row['car_model'];
	return($car_obj);
}

function display_customer_row($row,$cars_info)
{
    $encoded_cars = json_encode($cars_info);
	$cars_names =array();
    foreach($cars_info as $car_obj)
	{
	   $cars_names[] = $car_obj->name." ".$car_obj->model;
	}
	$cars_list = implode(",",$cars_names);
     
  	 
	$row_str = '<tr>';
	$row_str .= '<td>'.$row['id'].'</td>';
	$row_str .= '<td>'.$row['name'].'</td>';
	$row_str .= '<td>'.$row['details'].'</td>';
	$row_str .= '<td>'.$row['price'].'</td>';
	$row_str .= '<td>'.$cars_list.'</td>';
	$row_str .= '<td><a href="#" class="order_part" ';
	$row_str .= 'data-part_id="'.$row['id'].'" data-name="'.$row['name'].'" ';
	$row_str .= 'data-customer_id="'.$_SESSION['user_id'].'" ';
	$row_str .='data-desc="'.$row['details'].'" ';
	$row_str .='data-price="'.$row['price'].'" ';
	$row_str .='data-quantity="'.$row['quantity'].'" ';
	$row_str .='data-carsjson=\''.$encoded_cars.'\' ';
	$row_str .= '>Order</a></td>';
	$row_str .= '</tr>';
	echo $row_str;
		
}

if ( !empty($rows) ) { ?>
<h1 id="title"> Parts List </h1>

<div class="row">
	<div class="col-xs-7">
	<a href="<?php echo $VIEW_PARTS_URL; ?>"><button type="button" id="list_all" class="btn btn-primary btn-lg">Get All Parts</button></a>
	</div>
	<div class="col-xs-5">
	    <form id="search_form" class="form-inline pull-right col-xs-12" role="form" method="POST" action="view_parts">
		   <div class="form-group ui-widget" >
		        <input type="text" class="form-control ui-autocomplete-input" id="my_term" placeholder="Type name" autocomplete="off" >
				<input type="hidden" id="search_part_id" name="search_part_id" value="">				
		   </div>
		   <button type="button" id="search" class="btn btn-primary ">Search</button>
		   <span id="search_feedback" class="label label-default" style="display:none">Please select the search term from the automatic suggestions</span>
	    </form>
	</div>
</div>
<p></p>
<table class="table table-striped table-bordered table-hover" id="parts_container">
<tr id="titles" class="success">
         <th><a href="<?php echo $id_link; ?>">Id</a></th>
		 <th><a href="<?php echo $name_link; ?>">Name</a></th>
		 <th>Description</th>
		 <th><a href="<?php echo $price_link; ?>">Price</a></th>
		 <th>Compatible with:</th>
		 <th>Order</th>
</tr>

<?php
	$prev_id ="";
	$car_names = array();
	$cars_info = array();
	
    foreach($rows as $row)
	{ 
	    if (!empty($prev_id) )
        {	  		   		
		   $cars_info[] = get_car_object($prev_row);
		   if ($prev_id != $row['id'] )
			{			 
			  display_customer_row($prev_row,$cars_info);
			  $cars_info = array();
 			}			
	   }
	   $prev_id = $row['id'];
	   $prev_row = $row;
	} 		
	$cars_info[] = get_car_object($prev_row);
	display_customer_row($prev_row,$cars_info);
	?> 	
</table>
<?php 
 }
 else
 {
	echo "No items found";
 }
?>