
<h3>Registration</h3>
<?php if ( isset($error) ) { echo $error."<br>"; } ?> 

<form class="form-horizontal" role="form" method="POST" id="register" action="register"> 

<div class="form-group">
    <div class="col-sm-1"><label for="name" class="control-label ">Name:</label></div>
    <div class="col-sm-3">
         <input type="name" class="form-control" id="name" name="name" placeholder="Name">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-1"><label for="email" class="control-label ">Email:</label></div>
    <div class="col-sm-3">
         <input type="email" class="form-control" id="email" name="email" placeholder="Email">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-1"><label for="password" class="control-label">Password:</label></div>
    <div class="col-sm-3">
         <input type="password" class="form-control" id="passowrd" name="password" placeholder="Password">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-1"><label for="confirm_pass" class="control-label">Confirm Password:</label></div>
    <div class="col-sm-3">
         <input type="password" class="form-control" id="confirm_pass" name="confirm_pass" placeholder="Confirm Password">
    </div>
</div>

<div class="form-group">
  <div class="col-sm-1"><label for="my_role" class="control-label">Role:</label></div>
  <div class="col-sm-3">
			<select id="role" name="role" class="form-control">
			   <option value="">Please select your role</option>
			   <option value="CUSTOMER">Customer</option>
			   <option value="MANAGER">Manager</option>
			</select>
  </div>
</div>

<div class="form-group">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Register</button>
    </div>
  </div>
</form>
