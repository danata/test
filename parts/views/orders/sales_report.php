<?php
if ( !empty($rows) ) { ?>
<h4 id="title_best"> Best Selling Parts </h4>
<p></p>
<table class="table table-striped table-bordered table-hover" id="best_sellers">
<tr id="titles" class="success"><th>Id</th><th>Name</th><th>Quantity</th></tr>

<?php
	
    foreach($rows as $row)
	{ 
	  echo "<tr>";
	  echo "<td>".$row['id']."</td>";
	  echo "<td>".$row['name']."</td>";
	  echo "<td>".$row['quantity_sum']."</td>";
	  echo "</tr>";
	} 		
?> 	
</table>
<p></p>

<h4 id="title_all"> All Orders </h4>
<table class="table table-striped table-bordered table-hover" id="all_sales">
<tr id="sales_titles" class="success">
             <th><a href="<?php echo $id_link; ?>">Order Id</a></th>
			 <th><a href="<?php echo $date_link; ?>">Date</a></th>
			 <th><a href="<?php echo $part_id_link; ?>">Part Id</a></th>
			 <th><a href="<?php echo $name_link; ?>">Part Name</a></th>
			 <th><a href="<?php echo $customer_name_link; ?>">Customer</a</th>
			 <th><a href="<?php echo $quantity_link; ?>">Quantity</a></th>
			 <th><a href="<?php echo $sale_price_link; ?>">Sale Price</a></th>
</tr>
<?php 
  foreach($all_sales  as $sale )
  {
    echo "<tr>";
	echo "<td>".$sale['order_id']."</td>";
	echo "<td>".$sale['order_date']."</td>";
	echo "<td>".$sale['part_id']."</td>";
	echo "<td>".$sale['name']."</td>";
	echo "<td>".$sale['customer_name']."</td>";
	echo "<td>".$sale['quantity']."</td>";
	echo "<td>".$sale['sale_price']."</td>";
	echo "</tr>";
  }
?>
</table>

<?php 
 }
 else
 {
	echo "No items found";
 }
?>