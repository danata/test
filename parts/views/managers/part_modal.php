
<div class="modal fade" id="part_modal" tabindex="-1" role="dialog" aria-labelledby="partModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="partModalLabel"></h4>
      </div>
      <div class="modal-body">
        
		<div id="feedback"></div>
		<p>
		<form method="POST" id="part_form" action="update" class="form-horizontal" > 
		
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label text-primary">Name:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                </div>
            </div>
			
			<div class="form-group">
                <label for="desc" class="col-sm-2 control-label text-primary">Description:</label>
                <div class="col-sm-6">
                    <textarea class="form-control" id="desc" name="desc" placeholder="Description"> </textarea>
                </div>
            </div>
			
			<div class="form-group">
                <label for="price" class="col-sm-2 control-label text-primary">Price:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="price" name="price" placeholder="Price">
                </div>
            </div>
       		
			<div class="form-group">
                <label for="quantity" class="col-sm-2 control-label text-primary">Quantity:</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
                </div>
            </div>
			
			<div class="form-group">
                <label for="cars" class="col-sm-2 control-label text-primary">Compatible Cars:</label>
                <div class="col-sm-6">
				   <select id="cars" multiple class="form-control" rows="10">
				   
				   </select>				
                </div>
            </div>
		
			<input type="hidden" id="status" name="status" value="">
			<input type="hidden" id="action" name="action" value="">
			<input type="hidden" id="part_id" name="part_id" value="">
            <button type="submit" id="submit" class="btn btn-primary"></button>
        </form>

		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->