<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Car Parts</title>

    <!-- Bootstrap -->
    <link href="<?php echo $ROOT_PATH."css/bootstrap.min.css"; ?>" rel="stylesheet"> <!-- http://localhost/parts/css -->
	<link href="<?php echo $ROOT_PATH."css/jquery-ui.css"; ?>" rel="stylesheet">	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> 
	<script src="<?php echo $ROOT_PATH."js/jquery-3.2.1.min.js"; ?>" ></script>
	<script src="<?php echo $ROOT_PATH."js/jquery-ui.js"; ?>" ></script>
	<!--
	<script src="http://localhost/parts/js/jquery.ui.autocomplete.html.js" />
	
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.min.js"</script>
	-->
	<!-- <script src="http://localhost/test/parts/js/autocomplete_config.js"></script> -->
	<script src="<?php echo $ROOT_PATH."js/search.js"; ?>" ></script>
	
</head>
<body>
<div class="container">