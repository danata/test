<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header col-sm-12">
       <div class="row">
	       <div class="col-sm-8">
				<ul class="nav navbar-nav">
					<li><a href="#"><span class="label label-info" style="font-size:larger">Welcome <?php echo $_SESSION['name']; ?></span></a></li>
					<li><a href="<?php echo $VIEW_PARTS_URL;?>">View Parts</a></li>
					<?php if($_SESSION['user_type'] =='MANAGER'): ?>
					<li><a href="<?php echo $SALES_REPORT_URL;?>">Sales Report</a></li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="col-sm-2"></div>
			<div class="col-sm-2">
				<ul class="nav navbar-nav pull-left">
					<li><a href="<?php echo $LOGOUT_URL; ?>"><span class="label label-info" style="font-size:larger">Logout</font></a></li>
				</ul>
			</div>
	   </div>
  </div>
</nav>