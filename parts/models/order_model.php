<?php

class OrderModel
{
	private $db_obj;
   
   public function __construct()
   {
		global $SERVERNAME,$USERNAME,$PASSWORD,$DBNAME;
		
		$this->db_obj = MyDatabase::get_instance($SERVERNAME,$USERNAME,$PASSWORD,$DBNAME);
   }
   
   
   public function insert_order($part_id,$customer_id,$customer_name,$quantity,$price_val)
   { 
		$sale_price = $quantity * $price_val;
		$now = date("Y-m-d H:i:s");
		
		$query ="INSERT INTO `order` (`id`, `order_date`, `part_id`, `customer_id`, `customer_name`, `quantity`, `sale_price`) ";
		$query .=" VALUES (NULL, '".$now."', '".$part_id."', '".$customer_id."', '".$customer_name."', '".$quantity."', '".$sale_price."') ";
		error_log("Insert order query=".$query);
		
		$order_id = $this->db_obj->insert($query);
		        
		return($order_id);
   }
   
   public function get_best_sellers($limit=null)
   {
		$query = "SELECT part.id,part.name,SUM(order.quantity) as quantity_sum ";
		$query .= "FROM `order` JOIN `part` ON ( part.id = order.part_id ) ";
		$query .= "GROUP BY part.id ORDER BY SUM(order.quantity) DESC ";
		if ($limit)
		{
			$query .=" LIMIT ".$limit;
		}
		
		$rows = $this->db_obj->load_assoc_list($query);
		return($rows);		
   }
   
   public function get_all_sales($order_by='order_date',$dir='DESC')
   {
		$query = "SELECT order.id as order_id,order.order_date,part.id as part_id,part.name,order.customer_name,order.quantity,order.sale_price FROM `order` join `part` ON ( part.id = order.part_id )  ORDER BY ".$order_by." ".$dir;
	    error_log($query);
		$rows = $this->db_obj->load_assoc_list($query);
		return($rows);	
   }
}

?>