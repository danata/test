<?php

final class MyDatabase
{
   private $server;
   private $username;
   private $password;
   private $db_name;
   private $link;
   
   public static $instance = null;
   
   private function __construct($server,$username,$password,$db_name)
   {
      $this->server = $server;
	  $this->username = $username;
	  $this->password = $password;
	  $this->db_name  = $db_name;
	  $this->connect();
   }
   
   private function connect()
   {
      $this->link = mysqli_connect($this->server,$this->username,$this->password);
	  if (!$this->link) 
      {
            throw new Exception('Unable to establish database connection: ' 
                                .mysqli_connect_error());
      }
	   if (! mysqli_select_db($this->link,$this->db_name))
       {
            throw new Exception('Unable to select database: ' . mysqli_error($this->link));
       }
   }
   
   public function __clone()
   {
      triger_error("cloning is not allowed");
   }
   
   public static function get_instance($server,$username,$password,$db_name)
   {
      if ( ! isset(self::$instance) )
	  {
	     self::$instance = new MyDatabase($server,$username,$password,$db_name);
	  }
	  return(self::$instance);
   }
   
   public function get_one_value($query)
   {
      $result = mysqli_query($this->link,$query);
	  if (! $result) 
	  {
            throw new Exception("Query failed with error:".mysqli_error($this->link));
      }
	  $row = mysqli_fetch_array($result);
	  return($row[0]);
   }
   
   public function load_assoc_list ( $query )
   {
       $list = array();
       $result = mysqli_query($this->link,$query);
	   if (! $result) 
	   {
            throw new Exception("Query failed with error:".mysqli_error($this->link));
       }
	   while($row = mysqli_fetch_assoc($result))
	   {
	      $list[] = $row;
	   }
	   return($list);
   }
   
   public function query($query) 
    {
        $r = @mysqli_query($this->link,$query);

        if (!$r) {
            throw new Exception("Query Error: " . mysqli_error($this->link));
        }
        
        return $r;
    }
	
	public function insert($query) 
    {
        $r = $this->query($query);  
        return @mysqli_insert_id($this->link); 
    }
	
	public function updateOrDelete($query)
    {
        $r = $this->query($query);
        return @mysqli_affected_rows($this->link);
    }
    
}

?>