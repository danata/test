<?php
class PartModel
{
	private $db_obj;
   
   public function __construct()
   {
		global $SERVERNAME,$USERNAME,$PASSWORD,$DBNAME;
		
		$this->db_obj = MyDatabase::get_instance($SERVERNAME,$USERNAME,$PASSWORD,$DBNAME);
   }
   
   public function get_all_parts($part_id=null,$order_by='id',$dir='ASC')
   {
		$qorder = 'part.'.$order_by;
		$query="SELECT part . * , car.id AS car_id,car.name AS car_name, car.model AS car_model FROM part JOIN car_parts ";
		$query .="JOIN car ON ( part.id = car_parts.part_id AND car.id = car_parts.car_id ) ";
		$query .= "WHERE part.quantity > 0 ";
		if ($part_id)
		{
			$query .= " AND part.id='".$part_id."' ";
		}
		$query .= "ORDER BY ".$qorder." ".$dir;
			error_log($query);			
		$rows = $this->db_obj->load_assoc_list($query);
		return($rows);
   }
   
   public function get_matching_parts($term) // for the auto complete component
   {
		$query = "SELECT id,name FROM part WHERE (name LIKE '%".$term."%') ";
		$rows = $this->db_obj->load_assoc_list($query);
		
		$ui_rows = array();
		$ui_record = array();
		foreach($rows as $row)
		{
		  $ui_record["id"] = $row['id'];
		  $ui_record["value"] = $row['name'];
		  $ui_record["label"] = $row['name'];
		  array_push($ui_rows, $ui_record);
		}
		
		return($ui_rows); 		
   }
   
   public function get_all_cars()
   {
		$query ="SELECT * FROM car";
		$rows = $this->db_obj->load_assoc_list($query);
		return($rows);
   }
   
   public function update_part($info)
   {
		$query = "update part set name='".$info['name']."', details='".$info['desc']."', price='".$info['price']."',quantity='".$info['quantity']."' ";
		$query .= "WHERE id='".$info['part_id']."' ";
		$this->db_obj->query($query);
		
		$dquery = "DELETE from car_parts WHERE part_id='".$info['part_id']."'";
		$this->db_obj->query($dquery);
		
		$cars = $info["cars"];
		$this->insert_car_parts($info['part_id'],$cars);
		return(true); 
   }
   
   public function decriment_quantity($part_id,$sold_quantity)
   {
		$query ="UPDATE part set quantity=quantity-".$sold_quantity." WHERE id='".$part_id."'";
		error_log($query);
		$this->db_obj->query($query);
   }
   
   private function insert_car_parts($part_id,$cars)
   {
		foreach($cars as $car_id)
		{
			$query ="INSERT INTO car_parts SET part_id='".$part_id."',car_id='".$car_id."'";
			$this->db_obj->query($query);
		}
   }
   
   public function insert_part($info)
   {	
		$query = "INSERT INTO part set name='".$info['name']."', details='".$info['desc']."', price='".$info['price']."',quantity='".$info['quantity']."' ";
		$part_id = $this->db_obj->insert($query);
		$cars = $info["cars"];
		$this->insert_car_parts($part_id,$cars);
		return(true); 	
   }
}

?>