$(document).ready( function()
{

    $('#search').click( function(event)
	{
		event.preventDefault();
		var part_id = $('#search_part_id').val();
		
		if (part_id)
		{
		   $( "#search_form" ).submit();		   
		}
		else
		{
			$('#search_feedback').show();
	    }
	});
});