$(document).ready(function($){

    var search_url = $('#search_url').val();
	//alert("search_url="+search_url);

	/* 'get_matching_parts' */
    $('#my_term').autocomplete({
	source:search_url, 
	minLength:3,
	select: function(event,ui)
			{
			    part_id = ui.item.id;
				if (part_id !='')  // write down the selected search item, for later usage
				{
					$('#search_feedback').hide();   // erase comment if item was selected 
					$('#search_part_id').val(part_id);
				}				
			}
    });
});