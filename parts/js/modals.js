$(document).ready(function(){
  
  var view_parts_url = $('#view_parts_url').val();
  var add_url = $('#add_url').val();
  var update_url = $('#update_url').val();
  var order_url = $('#order_url').val();
  var get_cars_url = $('#get_cars_url').val();
  console.log("my cars url="+get_cars_url);
  
  $('#part_modal,#order_modal').on('hide.bs.modal', function (e) {
     
	 var status = $('#status').val();
	 if (status=="OK")
	 {			
			//window.location="view_parts";
			window.location = view_parts_url;
	 }
  })
  
  $('#add_part').click( function(event)  // trigger modal box of add part
  {
		event.preventDefault();
		$('#part_modal').modal({
							keyboard: true
						  });
		clear_form_data();
		$('#partModalLabel').text("Add Part");
		$('#submit').text("Add");
		$('#action').val("add");
		 build_cars_select();
								 
  });
  
  $(".order_part").click( function(event)  // triger modal box of order
  {
		event.preventDefault();
		
		var part_id = $(this).data('part_id');
		var name = $(this).data('name');
		var desc = $(this).data('desc');
		var price = $(this).data('price');
		var quantity = $(this).data('quantity');
		var cars = $(this).data('carsjson'); // already objects , no need to parse
					
		$('#order_modal').modal({
									keyboard: true
						  });
		
		var customer_id = $(this).data('customer_id');
		var part_id = $(this).data('part_id');
		var name = $(this).data('name');
		var desc = $(this).data('desc');
		var price = $(this).data('price');
		var quantity = $(this).data('quantity');
		var cars = $(this).data('carsjson'); // already objects , no need to parse
		var car_obj,cars_html;
		
		$('#part_name').text(name);
		$('#part_desc').text(desc);
		$('#price').text(price);
		$('#price_val').val(price);
		$('#avail_quantity').text(quantity);
		$('#part_id').val(part_id);
		$('#customer_id').val(customer_id);
		
		cars_html = '<ul>';
		for(i=0; i<cars.length ; i++)
		{
		    car_obj = cars[i];
			cars_html += '<li>';
			cars_html +=  car_obj.name+" "+car_obj.model;
			cars_html += '</li>';
		}
		cars_html += '</ul>';
		$('#compat_cars').html(cars_html);
  });
  
  $(".edit_part").click( function(event) // triger modal box for edit box
					  {
						event.preventDefault();
						
						var part_id = $(this).data('part_id');
						var name = $(this).data('name');
						var desc = $(this).data('desc');
						var price = $(this).data('price');
						var quantity = $(this).data('quantity');
						var cars = $(this).data('carsjson'); // already objects , no need to parse
					
						console.log(cars);
						
						  $('#part_modal').modal({
												keyboard: true
						  });
						  
						  $('#partModalLabel').text("Edit Part");
						  $('#submit').text("Update");
						  $('#name').val(name);
						  $('#desc').val(desc);
						  $('#price').val(price);
						  $('#quantity').val(quantity);
						  $('#part_id').val(part_id);
						  $('#action').val("update");
						   build_cars_select(cars);
								 
						  
					  }
      );  
	  
	  $('#order').click( function(event) 
	                       {
								event.preventDefault();
								
								var customer_name  = $('#customer_name').val();
								var quantity = $('#quantity').val();
								var part_id = $('#part_id').val();
								var customer_id = $('#customer_id').val();
								var price_val = $('#price_val').val();
								
								var data_obj = { "customer_name":customer_name,"quantity":quantity,"part_id":part_id,"customer_id":customer_id,"price_val":price_val};
								console.log("data_obj=");
								console.log(data_obj);
								
								$.ajax({
											type:'POST',
											data: { "params":data_obj },
											url: order_url,                             /* "../orders/order", */
											dataType:"json",
											success: feedback,							
											error: error_handler
										}); 
								
						   });
	  
	  $('#submit').click( function(event)
	                       {						       
								event.preventDefault();
								var part_id, action_url;								
								var action = $('#action').val();
								
								var name = $('#name').val();
							    var desc = $('#desc').val();
								var price = $('#price').val();
								var quantity = $('#quantity').val();
								var cars = $('#cars').val();
								
								if ( action=='update')
								{
								   part_id = $('#part_id').val();
								   action_url =  update_url; //'update';									
								}
								else
								{
									part_id="";
									action_url= add_url; //'add';									
								}
								  var obj = { "name":name,"desc":desc,"price":price,"quantity":quantity,"part_id":part_id,"cars":cars};
								 $.ajax({
											type:'POST',
											data: { "params":obj },
											url: action_url,
											dataType:"json",
											success: feedback,							
											error:error_handler											  
										}); 
								 
								 
						   });
						   
						   
						   
			
function build_cars_select(cars)
  {
     console.log("inside build, get_cars_url="+get_cars_url);
		$.ajax({
											type:'POST',
											url: get_cars_url, //"get_all_cars",
											dataType:"json",
											success: function(data)
													{
													  var i,obj;
													  var options="";
													  
															for(i=0; i< data.length; i++)
															{
															    obj = data[i];
																options +='<option value="'+obj.id+'" ';
																if (typeof cars !== 'undefined')
																{
																	for (j=0; j< cars.length; j++)
																	{
																	   if (obj.id == cars[j].id)
																	   {
																			options +=" selected ";
																	   }
																	}
																}
																options +='>'+obj.name+" "+obj.model+'</option>'
															}
															$('#cars').html(options);
													}
											,
											error:function (xhr, status, error)
											{
												  alert("ajax error handler:"+xhr.status+' '+xhr.responseText);											
											}     
										}); 
  }			
			
			
  });
  
  function feedback(data)
  {															
		if (data[0]=="O.K")
		{
			$('#feedback').text("The action has succeeded");
			$('#feedback').addClass("alert alert-success");															
			$('#status').val('OK');
		}
		else
		{
			$('#feedback').text("The action has failed");
			$('#feedback').addClass("lalert alert-danger");	
			$('#status').val('ERROR');
		}
		if (data[1])
		{
		    var avail_quantity = parseInt( $('#avail_quantity').text() );
			var order_quant = $('#quantity').val();
			
			avail_quantity -= order_quant;
			$('#avail_quantity').text(avail_quantity);
			
			$('#order_id').text("Your order id is:"+data[1]);
			$('#order_id').addClass("alert alert-success");			
		}
   }
   
   function error_handler(xhr, status, error)
   {
		alert(xhr.status+' '+xhr.responseText);	
		console.log("xhr.status="+xhr.status);
		console.log("xhr.responseText"+xhr.responseText);
   }
  
  function clear_form_data()
  {
		$('#name').val("");
		$('#desc').val("");
		$('#price').val("");
		$('#quantity').val("");
		$('#part_id').val("");
  }
  
  