<?php
include_once "models/my_database.php";
include_once "models/user_model.php";

class Users
{
	public function __construct()
    {
		$this->user_model = new UserModel();
	}
	
	public function index()
	{
	    global $LOGOUT_URL;
		global $LOGIN_URL;
		global $ROOT_PATH;
		
		error_log("in users index");
		$this->login();
	}
	
	public function register()
	{
	     global $LOGOUT_URL;
		global $LOGIN_URL;
		global $REGISTER_URL;
		global $ROOT_PATH;
		
		$args = func_get_args();
		if ( !empty($args[0]) )
		{
		   $params = $args[0];
			
		    $email = $params[0];
			$pass  = $params[1];
			$confirm_pas = $params[2];
			$role = $params[3];
			$name = $params[4];
			
			$user_obj = $this->user_model->get_user($email);
			if (!$user_obj) // if we don't already have such a user 
			{
				$user_id = $this->user_model->insert_user($email,$pass,$role,$name);
				$this->write_session_redirect($name,$email,$role);				
		    }
			else 
			{
				$error ="A user with the supplied email already exists. Please supply another email";
		    }
		}
		include_once 'views/header.php';
		include_once 'views/users_menu.php';
		include_once 'views/register.php'; 		
		include_once 'views/footer.php';
	}
	
	public function login()
	{
	    global $LOGOUT_URL;
		global $LOGIN_URL;
		global $REGISTER_URL;
		global $ROOT_PATH;
		
			error_log("In users:login");	
		$args = func_get_args();
		
		
		if ( !empty($args[0]) && !empty($args[0][0]) && !empty($args[0][1]))
		{
			$email = $args[0][0];
			$pass  = $args[0][1];
			$user_obj = $this->user_model->get_user($email,$pass);
			if ($user_obj)
			{
			   $this->write_session_redirect($user_obj['name'],$user_obj['email'],$user_obj['user_type'],$user_obj['id']);			
			}
			else // set error message
			{
			   $error="Wrong username or password ";			  
			}
		}
		include_once 'views/header.php';
		include_once 'views/users_menu.php'; 
		include_once 'views/login.php';
		include_once 'views/footer.php';
		
	}
	
	public static function logout()
	{  
	  global $PATH_TO_INDEX;
	  global $ROOT_PATH;
	  
	   error_log("in Users::logout 2");
	   unset($_SESSION['name']);
	   unset($_SESSION['email']);
	   unset($_SESSION['user_type']);
	   header("Location: ".$PATH_TO_INDEX."/users/");
		die();		
	}
	
	private function write_session_redirect($name,$email,$user_type,$user_id)
	{
	    global $PATH_TO_INDEX;
		global $ROOT_PATH;
		
		$_SESSION['name'] = $name;
		$_SESSION['email'] = $email;
		$_SESSION['user_type'] = $user_type;					
		$_SESSION['user_id'] = $user_id;
        header("Location: ".$PATH_TO_INDEX."/parts/view_parts");
		die();					
	}
}
?>