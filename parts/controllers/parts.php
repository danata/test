<?php
include_once "models/my_database.php";
include_once "models/part_model.php";
include_once "models/order_model.php";
include_once "controllers/users.php";

class Parts
{
    private $part_model;
	
	public function __construct()
    {
		$this->part_model = new PartModel();
	}
	
	public function view_parts() //$part_id=null
	{
	   global $ROOT_PATH;
	   global $PATH_TO_INDEX;
	   global $SEARCH_URL;
	   global $ADD_URL;
	   global $UPDATE_URL;
	   global $ORDER_URL ;
	   global  $VIEW_PARTS_URL ;
	   global $SALES_REPORT_URL;
	   global $GET_CARS_URL;
	   global $LOGIN_URL;
	   global $LOGOUT_URL;
	   
	   $args = func_get_args();
        
       if ( isset($args[0])  )
		{
		    if ( is_array($args[0]) )
			{
				$params = array_filter($args[0]); // sorting name/asc etc 
			}
			else
			{
				$params = $args[0]; //search part id
			}
	    }
	    
		$params_str = var_export($params,true);
		error_log("In view parts , params_str=".$params_str);
		
		$name_dir = 'asc';
		$price_dir = 'asc';
		$quantity_dir = 'asc';
		$id_dir = 'asc';
		
		if ( is_array($params) && !empty($params) )
		{
		   $order_by=$params[0];
		   $dir = $params[1];
		   
		   $rows = $this->part_model->get_all_parts("",$order_by,$dir);
		   $dir_value = $dir=='asc'?'desc':'asc'; // flip dir from asc to desc and vice versa depending on sent value
		   $field_name = $order_by."_dir"; // 
		   $$field_name = $dir_value;  // creates a variable like name_dir and sets it value
		}
		else 
		{					
			$part_id = $params;
			$rows = $this->part_model->get_all_parts($part_id);
		}
		$name_link = $PATH_TO_INDEX."/parts/view_parts/name/".$name_dir;
		$price_link = $PATH_TO_INDEX."/parts/view_parts/price/".$price_dir;
		$quantity_link= $PATH_TO_INDEX."/parts/view_parts/quantity/".$quantity_dir;
		$id_link = $PATH_TO_INDEX."/parts/view_parts/id/".$id_dir;
		
	    
		include_once 'views/header.php';
		include_once 'views/top_menu.php';
		if ($_SESSION['user_type']=='MANAGER')
		{
			include_once 'views/managers/view.php';
			include_once 'views/managers/part_modal.php';
		}
		else
		{
			include_once 'views/customers/customer_view.php';
			include_once 'views/customers/order_modal.php';
		}
		include_once 'views/footer.php';
	}
	
		
	public function logout()
	{  
		global $ROOT_PATH;
	     Users::logout();	
	}
	
	public function index()
	{
	   global $PATH_TO_INDEX;
	   
		header("Location: ".$PATH_TO_INDEX."/users/login/");
		die();	
	}
	
	public function update()
	{
	global $ROOT_PATH;
		$args = func_get_args();  
		if (!empty($args) )
		{
			$params = $args[0];
			
			$this->part_model->update_part($params);
			$res_array = array("O.K");		   
		    $json_data = json_encode($res_array);
		    header('Content-Type: application/json');
		    echo $json_data;
									
		}
	}
	
	public function add()
	{
	global $ROOT_PATH;
		$args = func_get_args();  
		if (!empty($args) )
		{
			$params = $args[0];			
			$this->part_model->insert_part($params);
			$res_array = array("O.K");		   
		    $json_data = json_encode($res_array);
		    header('Content-Type: application/json');
		    echo $json_data;				
		}
	}
	
	public function get_all_cars()
	{
	     $rows = $this->part_model->get_all_cars();
		 $json_data = json_encode($rows);
		 header('Content-Type: application/json');
         echo $json_data;
	}
	
	public function get_matching_parts($term) // for the auto complete
	{
		$rows = $this->part_model->get_matching_parts($term);
		$json_data = json_encode($rows);
		 header('Content-Type: application/json');
         echo $json_data;
	}
	public function get_certain_part($part_id)
	{		
		$row = $this->part_model->get_certain_part($part_id);
		if (!$row)
		{
			$row = array();
		}
		$json_data = json_encode($row);
		header('Content-Type: application/json');
        echo $json_data;
	}
		
}

?>