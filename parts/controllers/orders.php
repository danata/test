<?php
include_once "models/my_database.php";
include_once "models/part_model.php";
include_once "models/order_model.php";

class Orders
{
    private $order_model;
	
	public function __construct()
    {
		$this->order_model = new OrderModel();
		$this->part_model = new PartModel();
	}
	
	public  function order()
	{
	global $ROOT_PATH;
	    $args = func_get_args(); 
		if (!empty($args) )
		{
		   $params = $args[0];
		   $args_str=var_export($params,true);
		   $order_id = $this->order_model->insert_order($params["part_id"],$params["customer_id"],$params["customer_name"],$params["quantity"],$params["price_val"]);		   		   
		   $this->part_model->decriment_quantity($params["part_id"],$params["quantity"]);
		   $res_array = array("O.K",$order_id);		   
		   $json_data = json_encode($res_array);
		   header('Content-Type: application/json');
		   echo $json_data;
		}					   
	}
	
	public function sales_report()
	{
	global $ROOT_PATH;
		global $PATH_TO_INDEX;
	   global $SEARCH_URL;
	   global $ADD_URL;
	   global $UPDATE_URL;
	   global $ORDER_URL ;
	   global  $VIEW_PARTS_URL ;
	   global $SALES_REPORT_URL;
	   global $GET_CARS_URL;
	   global $LOGIN_URL;
	   global $LOGOUT_URL;
	   
		$rows = $this->order_model->get_best_sellers(3);
		$args = func_get_args(); 
		if ( isset($args[0])  )
		{
		    if ( is_array($args[0]) )
			{
				$params = array_filter($args[0]); // sorting name/asc etc 
			}
		}
		
		$order_by ='order_date';
		$dir ='desc';
		
		$order_id_dir = 'asc';
		$order_date_dir = 'desc';
		$part_id_dir ='asc';
		$name_dir ='asc';
		$customer_name_dir = 'asc';
		$quantity_dir ='asc';
		$sale_price_dir = 'asc';
		
		if ( is_array($params) && !empty($params) )
		{
		   $order_by=$params[0];
		   $dir = $params[1];
		   
		   $dir_value = $dir=='asc'?'desc':'asc'; // flip dir from asc to desc and vice versa depending on sent value
		   $field_name = $order_by."_dir"; // 
		   $$field_name = $dir_value;  // creates a variable like name_dir and sets it value
		}
		$id_link = $PATH_TO_INDEX."/orders/sales_report/order_id/".$order_id_dir;
		$date_link = $PATH_TO_INDEX."/orders/sales_report/order_date/".$order_date_dir;
		$part_id_link = $PATH_TO_INDEX."/orders/sales_report/part_id/".$part_id_dir;
		$name_link = $PATH_TO_INDEX."/orders/sales_report/name/".$name_dir;
		$customer_name_link = $PATH_TO_INDEX."/orders/sales_report/customer_name/".$customer_name_dir;
		$quantity_link = $PATH_TO_INDEX."/orders/sales_report/quantity/".$quantity_dir;
		$sale_price_link = $PATH_TO_INDEX."/orders/sales_report/sale_price/".$sale_price_dir;
		
		$all_sales = $this->order_model->get_all_sales($order_by,$dir);
		
		include_once 'views/header.php';
		include_once 'views/top_menu.php';
		include_once 'views/orders/sales_report.php';
		include_once 'views/footer.php';
	}
}
?>