<?php
include_once '../classes/employee.php';
include_once '../classes/developer.php';
include_once '../classes/tester.php';
include_once '../classes/system_admin.php';

try
{
	$all_week = array("Sunday","Monday","Tuesday","Wednesday","Thursday");
	$developer = new Developer("Ram","Software Developer",17500,"Programmers Room",$all_week,"01/01/2016","");
	$tester = new Tester("Gad","Qa Engineer","15000","Qa Room",$all_week,"01/01/2015","");
	$sys_admin = new SystemAdmin("Michael","System Admin","16000","Support Room",$all_week,"01/01/2014","");
	
	$workers = array($developer,$tester,$sys_admin);
	foreach($workers as $worker)
	{
		echo "<p>";
		$worker->hire();
		echo "<p>";
	}
	
	echo "Demonstrating bad initalization of object (missing name):";
	$bad_object = new Tester("","Qa Engineer","15000","Qa Room",$all_week,"01/01/2015",""); // demonstrating Exception throw and catch
}
catch(Exception $e)
{
     echo "<br>inside catch block";
	$msg = $e->getMessage();
	echo "<br>".$msg;
	error_log($msg);
}

?>
