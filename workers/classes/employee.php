<?php
abstract class RDEmployee
{
	private $name;
	private $position;
	private $salary;
	private $location;
	private $work_days;
	private $start_date;
	private $end_date;
	
	public function __construct($name,$position,$salary,$location,$work_days,$start_date,$end_date)
	{
	    if ( empty($name)  )
		{
		    throw new Exception("Name is a mandatory field and it was not supplied");
		}
		if ( empty($position) )
		{
			throw new Exception("Position is a mandatory field and it was not supplied");
		}
		if ( empty($salary) )
		{
			throw new Exception("Salary is a mandatory field and it was not supplied");
		}
		$this->set_name($name);
		$this->set_position($position);
		$this->set_salary($salary);
		$this->set_location($location);
		$this->set_work_days($work_days);
		$this->set_start_date($start_date);
		$this->set_end_date($end_date);
	}
	
	public function set_name($name)
	{
		$this->name = $name;
	}
	public function get_name()
	{
		return($this->name);
	}
	
	public function set_position($pos)
	{
		$this->position = $pos;
	}
	public function get_position()
	{
		return($this->position);
	}
	
	public function set_salary($salary)
	{
		$this->salary = $salary;
	}
	public function get_salary()
	{
		return($this->salary);
	}
	
	public function set_location($location)
	{
		$this->location = $location;
	}
	public function get_location()
	{
		return($this->location);
	}
	
	public function set_work_days($work_days)
	{
		$this->work_days = $work_days;
	}
	public function get_work_days()
	{
		return ($this->work_days);
	}
	
	public function set_start_date($start_date)
	{
		$this->start_date = $start_date;
	}
	public function get_start_date()
	{
		return($this->start_date);
	}
	
	public function set_end_date($end_date)
	{
		$this->end_date = $end_date;
	}
	public function get_end_date()
	{
		return($this->end_date);
	}
	
	public function hire()
	{
		echo "<br>Welcome to our staff, ".$this->name."<br>";
		echo "Please start your professional training following the next link:<br>";
		echo $this->professional_training();
	}
	
	public function fire()
	{
		echo "<br>Hello ".$this->name."<br>";
		echo "<br>This employment termination letter is to inform you that your employment with the company will end as of ".$this->end_date;
		echo "<br>Good luck in your new way.";
	}
	
	abstract public function professional_training();
}

?>